/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easy;

import java.util.Scanner;

/**
 *
 * @author Harish Bondalapati
 */
public class Oddities {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        String s="";
        for(int i=sc.nextInt();i>0;i--){
            int t = sc.nextInt();
            if(t%2==0)
                s+=t+" is even\n";
            else
                s+=t+" is odd\n";
        }
        System.out.println(s.trim());
    }
    
}
