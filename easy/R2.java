/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easy;

import java.util.Scanner;

/**
 *The number S is called the mean of two numbers R1 and R2 if S is equal to (R1+R2)/2. 
 * Mirko’s birthday present for Slavko was two integers R1 and R2. Slavko promptly 
 * calculated their mean which also happened to be an integer but then lost R2! Help Slavko restore R2.
 * 
 * @author Harish Bondalapati
 */
public class R2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println((-sc.nextInt())+2*sc.nextInt());
        
    }
    
}
